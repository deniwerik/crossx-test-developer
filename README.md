# Teste: Deni Werik Ribeiro Martins

### Prazo: 26/08/2019
##### *Antes de iniciar faça o fork do projeto e informe o prazo para a conclusão no primeiro commit. Ao finalizar abrir uma PR para Rodrigo Priolo(rodrigopriolo)

#Para rodar o projeto:

- Faça clone do repositório com git clone https://deniwerik@bitbucket.org/deniwerik/crossx-test-developer.git
- na pasta crossx renomeie o arquivo .env.example para .env e o edite com as seguintes configurações:


         APP_NAME=Crossx
    
         DB_HOST=mysql
         DB_DATABASE=crossx
         DB_USERNAME=root
         DB_PASSWORD=root
    
- Na pasta raiz do projeto, crossx-test-developer, faça o clone do container docker com o comando:

                
        git clone https://github.com/Laradock/laradock.git

- Na pasta laradock, edite o arquivo renomeie o arquivo .env-example para .env e edite o seguinte:


        APP_CODE_PATH_HOST=../crossx/
     
- Ainda na paste lararock, inicie o container com o comando:
 
 
        docker-compose up -d nginx mysql phpmyadmin redis workspace

- Acesse o phpMyAdmin pelo navegador em localhost:8080 e acesse o servidor mysql com usuario root e senha root

- Crie o banco de dados crossx

- Em seguida, no terminal execute:
 
 
        docker-compose exec workspace bash

- Então execute os comandos:
  
    
        composer update
          
        composer dump-autoload
          
        php artisan key:generate
                    
        php artisan migrate
      
- #####Deste modo já irá ser possivel rodar o projeto no navegador acessando no endereço localhost no navegador, no menu superior basta clicar em register, informar os dados e se cadastrar para acessar a aplicação

#####Obs: * Para rodar satisfatoriamente os testes com o PHPUnit, devido as chaves estrangeiras nas tabelas, recomendo a criação de um usuário e um contato utilizando o projeto, pois as factories, ContatoFactory e TelefoneFactory estão definidas como id nas chaves estrangeiras como 1
#####Obs2: * Procedimentos para o caso de acesso negado ao phpMyAdmin:

- No diretório laradock/mysql edite o arquivo my.cf e adicione a linha ao final de [mysqld]:

  
        default_authentication_plugin=mysql_native_password
        
- Execute o comando docker no terminal para iniciar os containers:


        docker-compose up -d nginx mysql phpmyadmin redis workspace

- No terminal vá até o diretório do projeto, entre no diretorio laradock e inicie o mysql no terminal com o comando:


        docker-compose exec mysql bash

- Entre na linha de comandos do mysql:


        mysql -u root -p

- Faça login como root (usuario e senha root)

- Execute os comandos:
  
  
        ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';
    
        ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'root';
    
        ALTER USER 'default'@'%' IDENTIFIED WITH mysql_native_password BY 'secret';
    
        CREATE DATABASE IF NOT EXISTS `crossx` COLLATE 'utf8_general_ci' ;
    
        GRANT ALL ON `crossx`.* TO 'root'@'%' ;
    
        FLUSH PRIVILEGES ;       
 

## Projeto
O projeto consiste em criar uma agenda de contatos parecida com a do android / IOS.
Deve possuir um CRUD de pessoas com soft delete, ou seja, nenhum dado deve ser excluído do banco.

## Desafio
Ao clicar para incluir ou editar o contato abrir o modal do contato e possibilitar:
	- Alterar nome da pessoa.
	- Incluir 1 ou mais telefones.
	- Excluir 1 ou mais telefones.
	- É possível que o contato tenha apenas o nome sem nenhum telefone.

Alterar foto do perfil do usuário logado:
	- IMPORTANTE: A imagem deve ser carregada, sem a necessidade de recarregar a página (usar ajax).

Na tela de contatos, possibilitar pesquisar por nome.

### Relatórios
	- Exibir todas as pessoas, que não possuem telefone cadastrado, ou seja, possuem apenas o nome, mas nenhum contato.
	- Exibir todas as pessoas que possuem telefone cadastrado.

### Requisitos
- Login (pode ser com um usuário gerado previamente);
- Todos os formulários (Inclusão, Edição) devem abrir num modal.
- Utilizar algum framework PHP (Laravel 5+, Yii2, Zend2, etc);
- A cada commit, colocar um descritivo da funcionalidade concluída / ajustada.

### Diferencial
- Utilizar máscaras nos campos.
- Questionar se realmente deseja excluir o registro.
- Exibir mensagens (flashes) na tela após cada ação. Ex: Contato adicionado com sucesso.
- Utilizar migrations.
- Utilizar dependêcias com o composer.
- Utilizar algum framework JS (Rect, Angular +2, Vue).
- Utilizar Docker.
- Utilizar Testes unitários.

### Template
Incluímos um template no repositório, não é obrigado utilizá-lo.


### Não esqueça
Não esqueça de editar este readme no final, nos dizendo como fazemos para rodar seu projeto em nossa máquina, e qual usuário devemos utilizar para logar no sistema

##### *Em caso de dúvidas, sinta-se à vontade para entrar em contato com [rodrigo.priolo@avecbrasil.com.br](rodrigo.priolo@avecbrasil.com.br).


    

    



