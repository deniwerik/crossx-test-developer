<?php

namespace App;

use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contato extends Model
{

    use SoftDeletes;
    use SoftCascadeTrait;
    //
    protected $fillable = ['nome', 'organizacao', 'email', 'grupos', 'endereco', 'user_id'];

    protected $softCascade = ['telefones'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function telefones()
    {
        return $this->hasMany('App\Telefone');
    }
}
