<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Telefone extends Model
{

    use SoftDeletes;
    //
    protected $fillable = ['telefone','contato_id'];

    public function contato()
    {
        return $this->belongsTo("App\Contato");
    }
}
