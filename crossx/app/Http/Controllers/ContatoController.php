<?php

namespace App\Http\Controllers;

use App\Contato;
use App\Telefone;
use App\Http\Requests\StoreContatoRequest;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class ContatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();
        $user_id = $user->id;
        $response = response()->json( Contato::where('user_id', $user_id)->get() );

        return view('home', compact('response'));
    }

    public function getContatos()
    {

        $user = Auth::user();

        $contatos = $user->contato;

        foreach ($contatos as $key => $contato){
            $contato->telefones;
        }

        $return["draw"] = 1;
        $return["recordsTotal"] = count($contatos);
        $return["recordsFiltered"] = count($contatos);
        $return["data"] = $contatos;

        header('Content-Type: application/json');
        echo json_encode($return);

    }

    public function contatosTelefones()
    {
        $user = Auth::user();

        $title = "Contatos com telefones";
        $url = "./get_contatos_telefones";

        return view('home', compact('user','title','url'));
    }

    public function getContatosTelefones()
    {
        $user = Auth::user();

        $contatos = Contato::whereHas('telefones', function ($query) use ($user) {
            $query->where('user_id', '=', $user->id);
        })->get();

        foreach ($contatos as $contato){
            $contato->telefones;
        }

        $return["draw"] = 1;
        $return["recordsTotal"] = count($contatos);
        $return["recordsFiltered"] = count($contatos);
        $return["data"] = $contatos;


        header('Content-Type: application/json');
        echo json_encode($return);

    }

    public function contatosSemTelefones()
    {
        $user = Auth::user();

        $title = "Contatos sem telefones";
        $url = "./get_contatos_sem_telefones";

        return view('home', compact('user','title','url'));
    }

    public function getContatosSemTelefones()
    {
        $user = Auth::user();

        $contatos = Contato::whereDoesntHave('telefones', function ($query) use ($user){
           $query->where('user_id', '=', $user->id);
        })->get();

        $return["draw"] = 1;
        $return["recordsTotal"] = count($contatos);
        $return["recordsFiltered"] = count($contatos);
        $return["data"] = $contatos;

        header('Content-Type: application/json');
        echo json_encode($return);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContatoRequest $request)
    {
        $input = $request->all();
        $input = $input["data"];

        $nome = $input[0];
        $organizacao = $input[1];
        $email = $input[2];
        $grupos = $input[3];
        $endereco = $input[4];

        $user = Auth::user();
        $user_id = $user->id;

        $contato = Contato::create([$nome['name'] => $nome['value'],
                                    $organizacao['name'] => $organizacao['value'],
                                    $email['name'] => $email['value'],
                                    $grupos['name'] => $grupos['value'],
                                    $endereco['name'] => $endereco['value'],
                                    'user_id' => $user_id]);

        if (count($input) >= 5 ){
            for($i = 5; $i < count($input); $i++){
                $tel = $input[$i];

                if (!empty($tel['value'])){
                    $telefone = Telefone::create([$tel['name'] => $tel['value'],
                        'contato_id' => $contato->id]);
                }

            }
        }

        $return['success'] = 'true';
        $return['msg'] = 'Contato adicionado com sucesso';

        header('Content-Type: application/json');
        echo json_encode($return);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contato  $contato
     * @return \Illuminate\Http\Response
     */
    public function show(Contato $contato)
    {
        //
    }

    /**
     * Retorna dados para serem editados
     *
     * @param  \App\Contato  $contato
     * @return \Illuminate\Http\Response
     */
    public function edit(Contato $contato)
    {

        $contato->telefones;

        header('Content-Type: application/json');
        echo json_encode($contato);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param    \App\Contato
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contato $contato)
    {

        $contatoData = $request->input("contato");
        $telefonesIds = $request->input("telefones");

        $nome = $contatoData[0]["value"];
        $organizacao = $contatoData[1]["value"];
        $email = $contatoData[2]["value"];
        $grupos = $contatoData[3]["value"];
        $endereco = $contatoData[4]["value"];

        $updateContato = $contato->update(["nome" => $nome, "organizacao" => $organizacao,
            "email" => $email, "grupos" => $grupos, "endereco" => $endereco]);
        $updateTelefone = true;

        if (count($contatoData) > 6){

            $telKey = 0;
            $contato_id = $contatoData[5]["value"];

            for ($i = 6; $i < count($contatoData); $i++){
                $tel = $contatoData[$i]["value"];
                if (!is_null($telefonesIds)){
                    $telefone = Telefone::findOrFail($telefonesIds[$telKey]);
                    $telefone->telefone = $tel;
                    $saveOrUpdateTelefone = $telefone->save();
                    if ($saveOrUpdateTelefone == false){
                        $updateTelefone = false;
                    }
                    $telKey++;
                } else {
                    $telefone = new Telefone();
                    $telefone->telefone = $tel;
                    $telefone->contato_id = $contato_id;
                    $saveOrUpdateTelefone = $telefone->save();
                    if ($saveOrUpdateTelefone == false){
                        $updateTelefone = false;
                    }
                }
            }
        }

        if ($updateContato == true && $updateTelefone == true){
            $return["success"] = "true";
            $return["msg"] = "Contato atualizado com sucesso";
        } else {
            $return["success"] = "false";
            $return["msg"] = "Falha ao atualizar contato";
        }

        header('Content-Type: application/json');
        echo json_encode($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Contato $contato
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy(Contato $contato)
    {
        $response = $contato->delete();

        if ($response == true){
            $return["success"] = "true";
            $return["msg"] = "Contato deletado com sucesso!";
        } else {
            $return["success"] = "false";
            $return["msg"] = "Erro ao deleter contato!";
        }

        header('Content-Type: application/json');
        echo json_encode($return);
    }


    public function testRead()
    {
        return response()->json(Contato::all());
    }

    public function testDelete(Contato $contato)
    {
        $contato->delete();
    }

    public function testUpdate(Request $request, Contato $contato)
    {
        $contato->update($request->all());
    }

    public function testCreate(Request $request)
    {
        Contato::create($request->all());
    }
}
