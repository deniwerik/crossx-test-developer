$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(".fileinput-upload-button").click(function (event) {
    var data = new FormData();
    jQuery.each(jQuery('#file')[0].files, function(i, file) {
        data.append('file[]', file);
    });

    let id = $("input[name='user_id']").val();
    data.append('id', id);

    $.ajax({
        url: './upload_photo',
        type: 'POST',
        contentType: false,
        cache: false,
        processData: false,
        method: 'POST',
        dataType: 'json',
        data: data,
        success: function(response){
            $("span.x-tree-icon-text").remove();
            $("#photo").removeClass("x-tree-icon-leaf");

            $("#photo").attr("src", response);
        },
        error : function(xhr ,status ,error)
        {

        },
    });

});

$(".fileinput-remove-button").click(function (event) {
    $("#file").val(null);
});

$(".altdrop").on('click', function (event) {
    $(this).parent().toggleClass('open');
});

$('body').on('click', function (e) {
    if (!$('li.dropdown.mega-dropdown').is(e.target)
        && $('li.dropdown.mega-dropdown').has(e.target).length === 0
        && $('.open').has(e.target).length === 0
    ) {
        $('li.dropdown.mega-dropdown').removeClass('open');
    }
});

function removePhone(el){

    $(el).closest(".form-group").remove();

}

$(".btn-add-contato").click(function () {


   var form = $("#form_new_contato").serializeArray();

    $.ajax({
        url: './contato',
        type: 'POST',
        data: {data: form},
        dataType: 'json',
        success: function (data) {

            if (data.success === "true"){
                let msg = data.msg;
                $.alert({
                    title: 'Contato atualizado',
                    content: msg,
                    buttons: {
                        confirm: {
                            text: 'OK',
                            action: function () {
                                location.reload();
                            }
                        },

                    }

                });
            } else {
                $.alert({
                    title: 'Erro',
                    content: 'Erro ao atualizar contato',
                    buttons: {
                        confirm: {
                            text: 'OK',
                            action: function () {
                                location.reload();
                            }
                        },

                    }

                });
            }

        }
    });
});

$(".btn-upd-contato").click(function () {
    var form = $("#form_edit_contato").serializeArray();
    var id = $("#edtcontatoid").val();

    var idsTelefones = [];
    $("input[name='edttelefone[]']").each(function(index, el) {
        idsTelefones.push($(this).data("idtelefoneedit")) ;
    });

    $.ajax({
        url: './contato/' + id,
        type: 'PUT',
        data: {
            contato: form,
            telefones: idsTelefones
        },
        dataType: 'json',
        success: function (data) {

            if (data.success === "true"){
                let msg = data.msg;
                $.alert({
                    title: 'Contato atualizado',
                    content: msg,
                    buttons: {
                        confirm: {
                            text: 'OK',
                            action: function () {
                                location.reload();
                            }
                        },

                    }

                });
            } else {
                $.alert({
                    title: 'Erro',
                    content: 'Erro ao atualizar contato',
                    buttons: {
                        confirm: {
                            text: 'OK',
                            action: function () {
                                location.reload();
                            }
                        },

                    }


                });
            }
        }
    });

});

$(document).ready(function () {

    $(".btn-add-tel").click(function (event) {

       var formGroup ='<div class="form-group">' +
                        '<label class="control-label">Telefone</label><br>' +
                            '<div class="row">' +
                                '<div class="col-md-11">' +
                                    '<input type="text" placeholder="" data-mask="(99) 99999-9999" class="form-control" name="telefone">' +
                                '</div>' +
                                '<div class="col-md-1">'+
                                '</div>' +
                            '</div>' +
                        '</div>';

        $("#form_new_contato").append(formGroup);

        const newBtn = $("<button type=\"button\" onclick=\"removePhone(this)\" class=\"btn btn-xs btn-link btn-remove-tel\"><i class=\"fa fa-minus-circle fa-2x\"></i></button>");

        $("#form_new_contato").find(".row").last().find('.col-md-1').append(newBtn);

    });

});

function addTelEdit() {

    var formGroup ='<div class="form-group">' +
        '<label class="control-label">Telefone</label><br>' +
        '<div class="row">' +
        '<div class="col-md-11">' +
        '<input type="text" placeholder="" data-mask="(99) 99999-9999" class="form-control" name="edttelefone[]">' +
        '</div>' +
        '<div class="col-md-1">'+
        '</div>' +
        '</div>' +
        '</div>';

    $("#form_edit_contato div.edttelefones").append(formGroup);

    const newBtn = $("<button type=\"button\" onclick=\"removePhone(this)\" class=\"btn btn-xs btn-link btn-remove-tel\"><i class=\"fa fa-minus-circle fa-2x\"></i></button>");

    $("#form_edit_contato div.edttelefones").find(".row").last().find('.col-md-1').append(newBtn);

}

function removeTelefone(el, edit=false) {

    let id = $(el).data("idtelefonedel");
    let key = $(el).data("key");
    let url = "./telefone/"+id;

    $.confirm({
        title: 'Remover telefone',
        content: 'Deseja remover este telefone para este contato?',
        buttons: {
            confirm: {
              text: 'Remover',
              action: function () {

                  $.confirm({
                      content: function(){
                          var self = this;
                          self.setTitle('');

                          return $.ajax({
                              url: url,
                              dataType: 'json',
                              type: 'DELETE'
                          }).done(function (response) {

                              if(response.success === "true"){
                                  if (key != 0){
                                      var qtDel = $(el).closest('tr').remove();

                                      if (qtDel.length < 1){
                                          $(el).closest('div.form-group').remove();
                                      }
                                  } else if(key == 0){
                                      $('input[name="edttelefone[]"]').val(null);
                                      $('input[name="edttelefone[]"]').removeAttr("data-idtelefoneedit");

                                  }

                              }
                              let msg = response.msg;

                              self.setContentAppend('<div>'+msg+'</div>');
                          }).fail(function(){
                              self.setContentAppend('<div>Falha!</div>');
                          });
                      },
                      onDestroy: function () {
                          if (edit === false){
                              location.reload();
                          }
                      }
                  });
              }
            },
            cancel: {
              text: 'Cancelar',
              action: function (){
                  return true;
              }
            }
        },

    });

}

function removeContato(el){

    let id = $(el).data("idcontatodel");

    let url = "./contato/"+id;

    $.confirm({
        title: 'Remover Contato',
        content: 'Deseja remover este contato?',
        buttons: {
            confirm: {
                text: 'Remover',
                action: function () {

                    $.confirm({
                        content: function(){
                            var self = this;
                            self.setTitle('');

                            return $.ajax({
                                url: url,
                                dataType: 'json',
                                type: 'DELETE'
                            }).done(function (response) {

                                if(response.success === "true"){

                                    $(el).closest('tr').remove();

                                }
                                let msg = response.msg;

                                self.setContentAppend('<div>'+msg+'</div>');
                            }).fail(function(){
                                self.setContentAppend('<div>Falha!</div>');
                            });
                        },
                        onDestroy: function () {
                        location.reload();
                    }
                    });
                }
            },
            cancel: {
                text: 'Cancelar',
                action: function (){
                    return true;
                }
            }
        }
    });

}

function editContato(el){

    $("#form_edit_contato")[0].reset();
    $("#form_edit_contato div.edttelefones").html(null);

    let id = $(el).data("idcontatoupd");
    let url = './contato/'+id+'/edit';

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $("#edtnome").val(data.nome);
            $("#edtorganizacao").val(data.organizacao);
            $("#edtemail").val(data.email);
            $("#edtgrupos").val(data.grupos);
            $("#edtendereco").val(data.endereco);
            $("#edtcontatoid").val(data.id);

            if(data.telefones.length > 0){

                $(".edttelefones").html(null);

                var addBtn = '';
                for(var i = 0; i < data.telefones.length; i++ ){
                    var formGroup ='<div class="form-group">' +
                        '<label class="control-label">Telefone</label><br>' +
                        '<div class="row">' +
                        '<div class="col-md-11">' +
                        '<input type="text" placeholder="" data-mask="(99) 99999-9999" class="form-control" name="edttelefone[]" data-idtelefoneedit="'+data.telefones[i].id+'" value="'+data.telefones[i].telefone+'">' +
                        '</div>' +
                        '<div class="col-md-1">';

                    if (i === 0){
                        addBtn = '<button type="button" onclick="addTelEdit()" class="btn btn-xs btn-link btn-add-tel-edit">'+
                            '<i class="fa fa-plus-circle fa-2x"></i>'+
                            '</button>';
                        formGroup += addBtn;
                    }

                    formGroup += '</div>' +
                        '</div>' +
                        '</div>';

                    $("#form_edit_contato div.edttelefones").append(formGroup);

                    const delBtn = $("<button type='button' onclick='removeTelefone(this,true)' name='deleteTelefone' data-idTelefoneDel='"+data.telefones[i].id+"' data-key='"+i+"' class='btn btn-default deleteTelefone'><i class='fa fa-trash'></i></button>");

                    $("#form_edit_contato div.edttelefones").find(".row").last().find('.col-md-1').append(delBtn);

                }
            } else if(data.telefones.length === 0){
                addBtn = '<div class="row">'+
                '<div class="col-md-11"></div>'+
                '<div class="col-md-1">'+
                 '<button type="button" onclick="addTelEdit()" class="btn btn-xs btn-link btn-add-tel-edit">'+
                    '<i class="fa fa-plus-circle fa-2x"></i>'+
                    '</button>'+
                    '</div>'+
                    '</div>';

                $("#form_edit_contato div.edttelefones").append(addBtn);
            }

        }
    });

    $("#myModalEdit").modal();

}

$("#myModalEdit").on('hide.bs.modal', function (e) {
    $("#form_edit_contato")[0].reset();
    $("#form_edit_contato div.edttelefones").html(null);
});
