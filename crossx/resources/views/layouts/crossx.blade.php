<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="slick, flat, dashboard, bootstrap, admin, template, theme, responsive, fluid, retina">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="" type="image/png">

    <title>{{$title}}</title>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/libs.css') }}" rel="stylesheet">

    <link href="{{ asset('css/import/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/import/bootstrap-reset.css') }}" rel="stylesheet">
    <link href="{{ asset('css/import/default-theme.css') }}" rel="stylesheet">
    <link href="{{ asset('css/import/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/import/icomoon-weather.css') }}" rel="stylesheet">
    <link href="{{ asset('css/import/jquery-ui-1.10.3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/import/simple-line-icons.css') }}" rel="stylesheet">

    <script src="{{ asset('js/jquery-3.4.1.js') }}"></script>

</head>

<body class="sticky-header">

<section>
    <!-- sidebar left start-->
    <div class="sidebar-left">
        <!--responsive view logo start-->
        <div class="logo dark-logo-bg visible-xs-* visible-sm-*">
            <a href="home">
                <img src="img/logo-icon.png" alt="">
                <!--<i class="fa fa-maxcdn"></i>-->
                <span class="brand-name">Crossx</span>
            </a>
        </div>
        <!--responsive view logo end-->

        <div class="sidebar-left-info">


            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked side-navigation">
                <li>
                    <h3 class="navigation-title">Navegação</h3>
                </li>
                <li><a href="/home"><i class="fa fa-home"></i> <span>Home</span></a></li>

                <li class="menu-list nav-active"><a href=""><i class="fa fa-book"></i> <span>Relatórios</span></a>
                    <ul class="child-list">
                        <li><a href="contatos-telefones"> Com telefone</a></li>
                        <li><a href="contatos-sem-telefones"> Sem telefone</a></li>

                    </ul>
                </li>


            </ul>
            <!--sidebar nav end-->

        </div>
    </div>
    <!-- sidebar left end-->

    <!-- body content start-->
    <div class="body-content" style="min-height: 1200px;">

        <!-- header section start-->
        <div class="header-section">

            <!--logo and logo icon start-->
            <div class="logo dark-logo-bg hidden-xs hidden-sm">
                <a href="index.html">
                    <img src="img/logo-icon.png" alt="">
                    <!--<i class="fa fa-maxcdn"></i>-->
                    <span class="brand-name">Crossx</span>
                </a>
            </div>

            <div class="icon-logo dark-logo-bg hidden-xs hidden-sm">
                <a href="index.html">
                    <img src="img/logo-icon.png" alt="">
                    <!--<i class="fa fa-maxcdn"></i>-->
                </a>
            </div>
            <!--logo and logo icon end-->

            <!--toggle button start-->
            <a class="toggle-btn"><i class="fa fa-outdent"></i></a>
            <!--toggle button end-->

            <div class="notification-wrap">

                <!--right notification start-->
                <div class="right-notification">
                    <ul class="notification-menu">


                        <li class="dropdown mega-dropdown">
                            <a href="javascript:;" class="btn btn-default dropdown-toggle altdrop" data-toggle="">
                                @if (isset($user->path))
                                    <img src="images/{{$user->path}}" alt="" id="photo">
                                @else
                                    <img alt="" id="photo" class="x-tree-icon-leaf">
                                    <span class="x-tree-icon-text"></span>
                                @endif


                                {{$user->name}}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu purple pull-right">
                                <li>
                                    <div class="input-group ">

                                        <div class="input-group-btn">

                                            <div class="btn btn-default btn-file wfa">
                                                <i class="glyphicon glyphicon-folder-open"></i> &nbsp;Selecione sua foto
                                                <input id="file" class="" type="file" name="file" accept="image/*">
                                                <input type="hidden" value="{{$user->id}}" name="user_id">
                                            </div>
                                            <br>

                                            <button type="button" title="Clear selected files" class="btn btn-default fileinput-remove fileinput-remove-button"><i class="glyphicon glyphicon-trash"></i> Remove</button>
                                            <button type="button" title="Abort ongoing upload" class="hide btn btn-default fileinput-cancel fileinput-cancel-button"><i class="glyphicon glyphicon-ban-circle"></i> Cancel</button>
                                            <button type="submit" title="Upload selected files" class="btn btn-default kv-fileinput-upload fileinput-upload-button pull-right"><i class="glyphicon glyphicon-upload"></i> Upload</button>

                                        </div>

                                    </div>
                                </li>

                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out pull-right"></i> Log Out
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>


                    </ul>
                </div>
                <!--right notification end-->
            </div>

        </div>
        <!-- header section end-->


        <!--body wrapper start-->
        <div class="wrapper">

            @yield('content')

        </div>
        <!--body wrapper end-->


        <!--footer section start-->
        <footer>
            2015 &copy; SlickLab by VectorLab.
        </footer>
        <!--footer section end-->

    </div>
    <!-- body content end-->
</section>

<!--common scripts for all pages-->
<script src="{{ asset('js/libs.js') }}"></script>

</body>
</html>
