@extends('layouts.crossx')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span>Contatos</span>

                    <div class="pull-right">
                        <!--modal start-->


                                <a class="btn btn-success" data-toggle="modal" href="#myModal">
                                    Adicionar Contato
                                </a>

                                <!-- Modal -->
                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">Contato</h4>
                                            </div>
                                            <div class="modal-body">

                                                <section class="panel">
                                                    <div class="panel-body">
                                                        <form role="form" id="form_new_contato">
                                                            <div class="form-group">
                                                                <label for="nome">Nome</label>
                                                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="organizacao">Organização</label>
                                                                <input type="text" class="form-control" id="organizacao" name="organizacao" placeholder="Organização">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="email">E-mail</label>
                                                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail">
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Grupos</label>

                                                                <div class="">

                                                                    <select class="form-control" id="grupos" name="grupos">
                                                                        <option></option>
                                                                        <option value="Contatos de Emergência">Contatos de emergência</option>
                                                                        <option value="Colegas de trabalho">Colegas de trabalho</option>
                                                                        <option value="Familia">Familia</option>
                                                                        <option value="Amigos">Amigos</option>
                                                                        <option value="Meus contatos">Meus contatos</option>

                                                                    </select>
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <label for="endereco">Endereço</label>
                                                                <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Endereço">
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Telefone</label><br>
                                                                <div class="row">
                                                                    <div class="col-md-11">
                                                                        <input type="text" placeholder="" data-mask="(99) 99999-9999" class="form-control" name="telefone">
                                                                    </div>
                                                                    <div class="col-md-1">
                                                                        <button type="button" class="btn btn-xs btn-link btn-add-tel">
                                                                            <i class="fa fa-plus-circle fa-2x"></i>
                                                                        </button>

                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </form>

                                                    </div>
                                                </section>

                                            </div>
                                            <div class="modal-footer">
                                                <button data-dismiss="modal" class="btn btn-default" type="button">Fechar</button>
                                                <button class="btn btn-success btn-add-contato" type="button">Salvar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- modal -->

                        <!--modal start-->
                    </div>

                </div>
                <br>

                <!-- panel body start -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <section class="panel">
                                <header class="panel-heading ">
                                    {{$title}}
                                </span>
                                </header>
                                <table id="rows-details-data-table" class="table display data-table rows-details-data-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Nome</th>
                                            <th>Organização</th>
                                            <th>E-Mail</th>
                                            <th>Grupos</th>
                                            <th>Endereço</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>Nome</th>
                                            <th>Organização</th>
                                            <th>E-Mail</th>
                                            <th>Grupos</th>
                                            <th>Endereço</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>

                                </table>
                            </section>
                        </div>

                    </div>
                </div>
                <!-- panel body end -->

                <!-- Modal Edit -->
                <div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Editar Contato</h4>
                            </div>
                            <div class="modal-body">

                                <section class="panel">
                                    <div class="panel-body">
                                        <form role="form" id="form_edit_contato">
                                            <div class="form-group">
                                                <label for="edtnome">Nome</label>
                                                <input type="text" class="form-control" id="edtnome" name="nome" placeholder="Nome">
                                            </div>

                                            <div class="form-group">
                                                <label for="edtorganizacao">Organização</label>
                                                <input type="text" class="form-control" id="edtorganizacao" name="organizacao" placeholder="Organização">
                                            </div>

                                            <div class="form-group">
                                                <label for="edtemail">E-mail</label>
                                                <input type="email" class="form-control" id="edtemail" name="email" placeholder="E-mail">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Grupos</label>

                                                <div class="">

                                                    <select class="form-control" id="edtgrupos" name="grupos">
                                                        <option></option>
                                                        <option value="Contatos de Emergência">Contatos de emergência</option>
                                                        <option value="Colegas de trabalho">Colegas de trabalho</option>
                                                        <option value="Familia">Familia</option>
                                                        <option value="Amigos">Amigos</option>
                                                        <option value="Meus contatos">Meus contatos</option>

                                                    </select>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label for="edtendereco">Endereço</label>
                                                <input type="text" class="form-control" id="edtendereco" name="endereco" placeholder="Endereço">
                                            </div>

                                            <input type="hidden" name="edtcontatoid" id="edtcontatoid" value="">


                                            <div class="edttelefones">

                                            </div>

                                        </form>

                                    </div>
                                </section>



                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Fechar</button>
                                <button class="btn btn-success btn-upd-contato" type="button">Atualizar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal -->

            </div>
        </div>
    </div>
</div>


<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var url = "{{$url}}";

    function format ( d ) {

        var telefones = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';

        $.each(d.telefones, function(index, val) {
            telefones += '<tr>' +
                '<td>Telefone: ' + val.telefone + '</td>' +
                '<td><button name="deleteTelefone" onclick="removeTelefone(this)" data-idTelefoneDel="'+val.id+'" class="btn btn-default deleteTelefone"><i class="fa fa-trash"></i></button></td>' +
                '</tr>';
        });

        telefones += '</table>';

        return telefones;

    }

    $(document).ready(function() {

        var dt = $('#rows-details-data-table').DataTable( {
            "processing": true,
            "serverSide": false,
            "ajax": url,
            "columns": [
                {
                    "class":          "details-control",
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": ""
                },
                { "data": "nome" },
                { "data": "organizacao" },
                { "data": "email" },
                { "data": "grupos" },
                { "data": "endereco" },
                {
                    "data": function (row,type,val,meta) {
                        let btnUpdate = "<button name='updateContato' onclick='editContato(this)' data-idContatoUpd='"+row.id+"' class='btn btn-default updateContato'><i class='fa fa-edit'></i></button>";
                        return btnUpdate;
                    },
                    "orderable":      false,
                },
                {

                    "data": function (row,type,val,meta) {
                        let btnDelete = "<button name='deleteContato' onclick='removeContato(this)' data-idContatoDel='"+row.id+"' class='btn btn-default deleteContato'><i class='fa fa-trash'></i></button>";

                        return btnDelete;
                    },
                    "orderable":      false,

                }

            ],
            "order": [[1, 'asc']]
        } );

        // Array to track the ids of the details displayed rows
        var detailRows = [];

        $('#rows-details-data-table tbody').on( 'click', 'tr td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = dt.row( tr );
            var idx = $.inArray( tr.attr('id'), detailRows );

            if ( row.child.isShown() ) {
                tr.removeClass( 'details' );
                row.child.hide();

                // Remove from the 'open' array
                detailRows.splice( idx, 1 );
            }
            else {
                tr.addClass( 'details' );
                row.child( format( row.data() ) ).show();

                // Add to the 'open' array
                if ( idx === -1 ) {
                    detailRows.push( tr.attr('id') );
                }
            }
        } );

        // On each draw, loop over the `detailRows` array and show any child rows
        dt.on( 'draw', function () {
            $.each( detailRows, function ( i, id ) {
                $('#'+id+' td.details-control').trigger( 'click' );
            } );
        } );
    } );

</script>
@endsection
