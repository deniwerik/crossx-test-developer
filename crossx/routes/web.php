<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::middleware(['auth'])->group(function(){

    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('contato','ContatoController');

    Route::post("upload_photo", "UserController@upload");

    Route::resource('user', 'UserController');
    Route::resource('telefone', 'TelefoneController');

    Route::get('get_contatos', 'ContatoController@getContatos');

    Route::get('get_contatos_telefones', 'ContatoController@getContatosTelefones');
    Route::get('get_contatos_sem_telefones', 'ContatoController@getContatosSemTelefones');

    Route::get('contatos-telefones', 'ContatoController@contatosTelefones');
    Route::get('contatos-sem-telefones', 'ContatoController@contatosSemTelefones');

});

Route::get('teste_read_telefone', 'TelefoneController@testRead');
Route::delete('teste_delete_telefone/{telefone}', 'TelefoneController@testDelete');
Route::post('teste_update_telefone/{telefone}', 'TelefoneController@testUpdate');
Route::post('teste_create_telefone', 'TelefoneController@testCreate');

Route::get('teste_read_contato', 'ContatoController@testRead');
Route::delete('teste_delete_contato/{contato}', 'ContatoController@testDelete');
Route::post('teste_update_contato/{contato}', 'ContatoController@testUpdate');
Route::post('teste_create_contato', 'ContatoController@testCreate');



