let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.webpackConfig({
    watchOptions: {
        ignored: /node_modules/
    }
});

mix.styles([

    'resources/assets/css/libs/slidebars.css',
    'resources/assets/js/libs/switchery/switchery.min.css',
    'resources/assets/css/libs/style.css',
    'resources/assets/css/libs/style-responsive.css',
    'resources/assets/js/libs/bootstrap-fileinput-master/css/fileinput.css',
    /*'resources/assets/js/libs/data-table/css/jquery.dataTables.css',
    'resources/assets/js/libs/data-table/css/dataTables.tableTools.css',
    'resources/assets/js/libs/data-table/css/dataTables.colVis.min.css',
    'resources/assets/js/libs/data-table/css/dataTables.responsive.css',
    'resources/assets/js/libs/data-table/css/dataTables.scroller.css',*/
    'resources/assets/css/libs/datatables.css',
    'resources/assets/js/libs/gritter/css/jquery.gritter.css',
    'resources/assets/css/libs/jquery-ui.css',
    'resources/assets/css/libs/jquery-ui.structure.css',
    'resources/assets/css/libs/jquery-ui.theme.css',
    'resources/assets/css/libs/jquery-confirm.min.css',
    'resources/assets/css/libs/crossx.css'

], 'public/css/libs.css');

mix.scripts([

    /*'resources/assets/js/libs/jquery-3.4.1.js',*/
    'resources/assets/js/libs/jquery-confirm.js',
    'resources/assets/js/libs/jquery-ui.js',
    'resources/assets/js/libs/jquery-migrate.js',
    'resources/assets/js/libs/bootstrap.min.js',
    'resources/assets/js/libs/modernizr.min.js',
    'resources/assets/js/libs/jquery.nicescroll.js',
    'resources/assets/js/libs/slidebars.min.js',
    'resources/assets/js/libs/switchery/switchery.min.js',
    'resources/assets/js/libs/switchery/switchery-init.js',
    'resources/assets/js/libs/sparkline/jquery.sparkline.js',
    'resources/assets/js/libs/sparkline/sparkline-init.js',
    'resources/assets/js/libs/scripts.js',
    'resources/assets/js/libs/bootstrap-fileinput-master/js/fileinput.js',

    /*'resources/assets/js/libs/data-table/js/jquery.dataTables.min.js',
    'resources/assets/js/libs/data-table/js/dataTables.tableTools.min.js',
    'resources/assets/js/libs/data-table/js/bootstrap-dataTable.js',
    'resources/assets/js/libs/data-table/js/dataTables.colVis.min.js',
    'resources/assets/js/libs/data-table/js/dataTables.responsive.min.js',
    'resources/assets/js/libs/data-table/js/dataTables.scroller.min.js',
    'resources/assets/js/libs/data-table-init.js',*/

    'resources/assets/js/libs/datatables.js',
    'resources/assets/js/libs/jquery.pulsate.min.js',
    'resources/assets/js/libs/pulstate.js',
    'resources/assets/js/libs/gritter/js/jquery.gritter.js',
    'resources/assets/js/libs/gritter.js',
    'resources/assets/js/libs/bs-input-mask.min.js',

    'resources/assets/js/libs/crossx.js'

],'public/js/libs.js');
