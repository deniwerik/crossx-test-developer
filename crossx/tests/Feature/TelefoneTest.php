<?php

namespace Tests\Feature;

use App\Telefone;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TelefoneTest extends TestCase
{
    use DatabaseTransactions;
    
    /** @test */
    public function create_telefone()
    {
        $telefone = factory(Telefone::class)->make();

        $this->post('teste_create_telefone', $telefone->toArray());


        $this->assertEquals(2,Telefone::count());
    }

    /** @test */
    public function update_telefone()
    {
        $telefone = factory(Telefone::class)->create();

        $this->post('teste_update_telefone/' . $telefone->id, ['telefone' => '(00)0000-0000']);

        $this->assertEquals('(00)0000-0000', Telefone::find($telefone->id)->telefone);
    }

    /** @test */
    public function delete_telefone()
    {
        $telefone = factory(Telefone::class)->create();

        $this->delete('teste_delete_telefone/' . $telefone->id);

        $this->assertEquals(1,Telefone::count());
    }

    /** @test */
    public function read_telefone()
    {
        $telefones = factory(Telefone::class, 20)->create();

        $response = $this->get('teste_read_telefone');

        $this->assertEquals(21, count($response->json()));
    }
}
