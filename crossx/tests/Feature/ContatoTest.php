<?php

namespace Tests\Feature;

use App\Contato;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContatoTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function create_contato()
    {
        $contato = factory(Contato::class)->make();

        $this->post('teste_create_contato', $contato->toArray());

        $this->assertEquals(2,Contato::count());
    }

    /** @test */
    public function update_contato()
    {
        $contato = factory(Contato::class)->create();

        $this->post('teste_update_contato/' . $contato->id, ['nome' => 'Toadette']);

        $this->assertEquals('Toadette', Contato::find($contato->id)->nome);
    }

    /** @test */
    public function delete_contato()
    {
        $contato = factory(Contato::class)->create();

        $this->delete('teste_delete_contato/' . $contato->id);

        $this->assertEquals(1,Contato::count());
    }

    /** @test */
    public function read_contato()
    {
        $contatos = factory(Contato::class, 20)->create();

        $response = $this->get('teste_read_contato');

        $this->assertEquals(21, count($response->json()));
    }
}
