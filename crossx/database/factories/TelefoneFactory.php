<?php

use Faker\Generator as Faker;

$factory->define(App\Telefone::class, function (Faker $faker) {
    return [
        'telefone' => $faker->tollFreePhoneNumber,
        'contato_id' => 1
    ];
});
