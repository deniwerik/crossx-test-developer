<?php

use Faker\Generator as Faker;

$factory->define(App\Contato::class, function (Faker $faker) {
    return [
        'nome' => $faker->name,
        'organizacao' => $faker->company,
        'email' => $faker->email,
        'grupos' => $faker->catchPhrase,
        'endereco' => $faker->address,
        'user_id' => 1

    ];
});
